﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GuYuGplot.Entites;

namespace TestMain
{
    public partial class XiaWCForm : Form
    {
        public XiaWCForm()
        {
            InitializeComponent();
            this.Add(12);
            this.Add(12);
            this.Add(12);
        }

        private void Add(int count)
        {
            GuYuGplot.Gplot gplot = new GuYuGplot.Gplot();
            gplot.Width = this.Height;
            gplot.Dock = DockStyle.Left;
            this.Controls.Add(gplot);

            Node rootNode = new Node();
            rootNode.Text = "Root";
            rootNode.Width = 50;
            rootNode.Height = 50;
            rootNode.NodeType = Enums.NodeType.Circle;

            for (int i = 0; i < count; i++)
            {
                Node cNode = new Node();
                cNode.Text = "C" + i.ToString();
                cNode.Color = Color.Red;
                cNode.Width = 40;
                cNode.Height = 40;
                cNode.NodeType = Enums.NodeType.Circle;

                Line line = new Line();
                line.Color = Color.RoyalBlue;
                line.Length = 80;
                cNode.Line = line;

                rootNode.ChildNodes.Add(cNode);
            }

            gplot.NodesTree.RootNode = rootNode;
        }
    }
}
