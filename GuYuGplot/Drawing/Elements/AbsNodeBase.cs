﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GuYuGplot.Drawing.Elements
{
    abstract class AbsNodeBase
    {
        public PointF Location { get; set; }
        public SizeF Size { get; set; }
        public Color Color { get; set; }
        public String Text { get; set; }
        public Color TextColor { get; set; }
        public int TextSize { get; set; }
        public string NodeName { get; set; }
    }
}
